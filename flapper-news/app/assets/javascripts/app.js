var myApp = angular.module('flapperNews',['ui.router']);

myApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  function($stateProvider,$urlRouterProvider){

    $urlRouterProvider.otherwise('home');

    $stateProvider
      .state('home', {
        url:'/home',
        templateUrl:'/home.html',
        controller:'MainCtrl'
      })

      .state('posts', {
        url:'/posts/{id}',
        templateUrl: '/posts.html',
        controller: 'PostsCtrl'
      })

  }]);

myApp.factory('posts',[function(){
  //Service Body
  var o = {posts: []};
  return o;
}]);

myApp.controller('MainCtrl',[
  '$scope',
  'posts',
  function($scope,posts){
  $scope.test = "hellow world!";
  $scope.posts = posts.posts;
  // $scope.posts = [
  //   {title: 'post 1', upvotes: 5},
  //   {title: 'post 2', upvotes: 2},
  //   {title: 'post 3', upvotes: 15},
  //   {title: 'post 4', upvotes: 9},
  //   {title: 'post 5', upvotes: 4}
  // ];
  $scope.addPost = function(){
    if(!$scope.title || $scope.title===''){return;}
    comments = [
    {author: 'Joe', body: 'Cool post!', upvotes: 0},
    {author: 'Bob', body: 'Bad  post!', upvotes: 0}
    ];
    $scope.posts.push({title: $scope.title, link: $scope.link, upvotes: 0,comments});
    $scope.title = '';
    $scope.link = '';
  };
  $scope.incrementUpvotes = function(post){
    post.upvotes += 1;
  };
}]);

myApp.controller('PostsCtrl',[
  '$scope',
  '$stateParams',
  'posts',
  function($scope,$stateParams,$posts){
    //Controller Content
    $scope.post = posts.posts[$stateParams.id];
    $scope.addComment = function(){
      if($scope.body === '') {return;}
      $scope.post.comments.push({
        body: $scope.body,
        author: 'user',
        upvotes: 0
      });
      $scope.body = '';
    };
  }]);
